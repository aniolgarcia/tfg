#!/bin/bash

tlmgr option repository ftp://tug.org/historic/systems/texlive/2021/tlnet-final  #Els repos de CTAN fan servir la versió de texlive 2022 però el meu container encara no està actualitzat
tlmgr update --self
tlmgr install \
    latexmk \
    filecontents \
    bytefield \
    standalone \
    pstricks \
    pst-tree \
    pst-node \
    xstring \
    preview \
    listingsutf8 \
    contour \
    tcolorbox \
    tocbibind \
    preprint \
    babel-catalan \
    biblatex \
    biber \
    todonotes \

